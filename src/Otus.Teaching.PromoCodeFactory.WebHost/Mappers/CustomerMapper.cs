using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class CustomerMapper
    {
        public static Customer MapFromModel(CreateOrEditCustomerRequest request, 
            IEnumerable<Preference> preferences, Customer customer = null)
        {
            if (customer == null)
            {
                customer = new Customer
                {
                    Preferences = new List<CustomerPreference>(),
                    PromoCodes = new List<PromoCodeCustomer>(),
                };
            }
            
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences?.Clear();
            if (preferences?.Any() == false)
                return customer;
            
            customer.Preferences = preferences
                .Select(x => new CustomerPreference(customer, x))
                .ToList();

            return customer;
        }
    }
}