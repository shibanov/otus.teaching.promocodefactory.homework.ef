﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }

        public PreferenceResponse()
        {
        }

        public PreferenceResponse(Preference preference)
        {
            if (preference == null)
                throw new ArgumentNullException(nameof(preference));

            Id = preference.Id;
            Name = preference.Name;
        }
    }
}